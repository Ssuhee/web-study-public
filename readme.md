# Study for (API, storage, auth) WEB

## 1) IoT communication APIs
In IoT, there are 2 communication APIs:
- >REST Based Communication APIs

    REpresentational State Transfer (REST). Request-response communication model.
    - XMLHttpRequest
        - Oldest one and compatibile on almost every browser (include IE11)
        - Event based == Callback hell. (But we use as promise based)
        - Can handle almost everything related REST API
        - Available on Web workers
        - Not available on Service workers
        - Not available on Node
    - fetch: 
        - Newly create (new standard of modern browser), but under development
        - Promise based == Simple syntax
        - Can't handle request progress, abort, different content-type, ...
        - Available on Web workers
        - Available on Service workers
        - Experimental feature from Node v18
    - axios (third party library, it uses xhr inside)
        - Most popular
        - Promise based == Simple syntax
        - Can handle almost everything related REST API
        - Available on Web workers (additional setup required)
        - Available on Service workers (additional setup required)
        - Available on Node
    - Swr (third party library): recommended use with react family (ex: React, Next, Remix).
        - Cache response certain amount time. It resolve duplication and race condition. And it uses one of above as fetcher. Which means can't compare it to other one.
        - We can do similiar thing with above one. (Cache-Control: stale-while-revalidate)
- >Web Socket Based Communication APIs

    Similiar to "polling", but it's more advanced technology. Two-way interactive communication which allow real-time things.
    - websocat (unicage)
    - net (node.js)          --> (default package)
    - socket (python)        --> (default package)

## 2) Browser Storage
- >Local Storage

    Characteristic:
    - Key-value storage that stores values as strings
    - Does not have expiration date (persistent storage) unless explicitly clear the browser using settings or Javascript
    - Up to 10MB data can be stored
    - Follow the same-origin policy, which means the Protocol(Http/Https), port and the host are the same. Only scripts of the same origin can access LocalStorge data
    - Do not send to server, for client-side usage only

    Use case:
    - Can be use to store user-related data

- >Session Storage

    Characteristic:
    - Key-value storage that stores values as strings
    - Data stored does not survive after the table/window is closed
    - Up to 10MB data can be stored
    - Follow the same-origin policy and is bound to a tab
    - Do not send to server, for clien-side usage only

    Use case:
    - Store user-related data for one session only like language selection

- >IndexedDB

    Characteristic:
    - Can store both objects and key-value pairs
    - Up to 250MB for IE
    - IndexedDB API is asynchronous, unlike loca storage and session storage. IndexedDB operations are event-driven by various events like onsuccess, onerror, oncomplete etc.
    - Follow the same-origin policy
    - Do not have expiration time (persistent storage) unless explicit deletion

    Use case:
    - When need to store a large number of objects which is time-consuming and a lag on performance to convert to string for Local Storage every time.


- >Shared Storage (Experimental)

## 3) Web Auth

## 4) Reference links:
- https://www.geeksforgeeks.org/difference-between-rest-api-and-web-socket-api/
- https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest
- https://stackoverflow.com/questions/54828368/how-use-axios-in-web-worker-with-vue#:~:text=You%20can't%20use%20axios,bundle%20the%20worker%20by%20itself.
- https://developer.mozilla.org/en-US/docs/Web/API/fetch
- https://axios-http.com/docs/intro
- https://medium.com/adg-vit/xhr-vs-fetch-vs-ajax-vs-axios-for-api-requests-f06e6bd56b32
- https://swr.vercel.app/
- https://datatracker.ietf.org/doc/html/rfc5861
- https://bootcamp.uxdesign.cc/understanding-multithreading-mutation-concurrency-race-condition-and-deadlock-in-javascript-432e8bc5ddac
- https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API
- https://medium.com/the-developer-journal/10-most-amazing-use-cases-of-websockets-go-real-time-166b71e0e711
- https://medium.com/@lancelyao/browser-storage-local-storage-session-storage-cookie-indexeddb-and-websql-be6721ebe32a
- https://developer.mozilla.org/en-US/docs/Web/API/Shared_Storage_API
