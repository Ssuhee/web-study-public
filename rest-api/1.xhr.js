export const f_xhr = (method, url, onSuccess, onError) => {
  const xhr = new XMLHttpRequest();
  xhr.open(method, url);
  xhr.responseType = "json";
  xhr.onload = () => onSuccess(xhr.response);
  xhr.onerror = () => onError(xhr.response);
  xhr.send();
  
};

export const f_xhrPromiseBased = (method, url) => {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.responseType = "json";
    xhr.onload = () => resolve(xhr.response);
    xhr.onerror = () => reject(xhr.response);
    xhr.send();
  });
};
