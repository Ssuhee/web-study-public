export const f_fetch = (method, url) => {
  return fetch(url, { method });
};