export const f_axios = (method, url) => {
  const methodLowerCase = `${method}`.toLowerCase();
  return axios[methodLowerCase](url);
};
