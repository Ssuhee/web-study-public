import { f_xhr, f_xhrPromiseBased } from "./1.xhr.js";
import { f_fetch } from "./2.fetch.js";
import { f_axios } from "./3.axios.js";

document.addEventListener("DOMContentLoaded", () => {
  const baseURL = "https://jsonplaceholder.typicode.com";
  const options = [
    {
      name: "xhr",
      func: () => {
        f_xhr("GET", baseURL + "/posts", onSuccess, onError);
      },
    },
    {
      name: "xhr-promise",
      func: () => {
        f_xhrPromiseBased("GET", baseURL + "/posts")
          .then(onSuccess)
          .catch(onError);
      },
    },
    {
      name: "fetch",
      func: () => {
        f_fetch("GET", baseURL + "/posts")
          .then((res) => res.json())
          .then(onSuccess)
          .catch(onError);
      },
    },
    {
      name: "axios",
      func: () => {
        f_axios("GET", baseURL + "/posts")
          .then((res) => res.data)
          .then(onSuccess)
          .catch(onError);
      },
    },
  ];

  const selectElement = document.getElementById("setState");
  const postListElement = document.getElementById("post-list");
  const statusElement = document.querySelector("footer > p");

  const handleSelectChange = ({ target: { value } }) => {
    options[value].func();
  };
  
  const loadPost = (posts) => {
    posts.forEach(({ title, body }) => {
      const postElement = document
        .getElementById("post")
        .content.cloneNode(true);
      postElement.querySelector("[data-title]").textContent = title;
      postElement.querySelector("[data-body]").textContent = body;
      postListElement.append(postElement);
    });
    statusElement.textContent = `Status: ${postListElement.children.length}`;
  };

  const loadOptions = () => {
    options.forEach(({ name },key) => {
      const optionElement = document.createElement("option");
      optionElement.value = key;
      optionElement.textContent = name;
      selectElement.append(optionElement);
    });
    selectElement.addEventListener("change", handleSelectChange);
    selectElement.dispatchEvent(new Event("change"));
  };

  const onError = (response) => {
    alert(response);
  };
  const onSuccess = (response) => {
    loadPost(response);
  };
  const onFinished = () => {
    alert("successfully finished!");
  };

  loadOptions();
});
