const net = require('net');
const crypto = require('crypto');

let clients = [];

const server = net.createServer((socket) => {
    socket.on('data', (data) => {
        if (data.toString().includes('Sec-WebSocket-Key')) {
            // Perform WebSocket handshake
            const headers = parseHeaders(data.toString());
            const acceptKey = createAcceptKey(headers['Sec-WebSocket-Key']);
            const response = [
                'HTTP/1.1 101 Switching Protocols',
                'Upgrade: websocket',
                'Connection: Upgrade',
                `Sec-WebSocket-Accept: ${acceptKey}`,
                '',
                ''
            ].join('\r\n');
            socket.write(response);
            clients.push(socket);
        } else {
            // Handle WebSocket frame
            const message = decodeMessage(data);
            if (message) {
                broadcastMessage(message);
            }
        }
    });

    socket.on('end', () => {
        clients = clients.filter((client) => client !== socket);
    });
});

server.listen(65431, '127.0.0.1', () => {
    console.log('Server is running on ws://127.0.0.1:65431');
});

function parseHeaders(request) {
    const headers = {};
    const lines = request.split('\r\n');
    for (let line of lines) {
        const parts = line.split(': ');
        if (parts.length === 2) {
            headers[parts[0]] = parts[1];
        }
    }
    return headers;
}

function createAcceptKey(key) {
    const magicString = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';
    const hash = crypto.createHash('sha1').update(key + magicString).digest('base64');
    return hash;
}

function decodeMessage(data) {
    const length = data[1] & 127;
    let maskStart, dataStart;
    if (length === 126) {
        maskStart = 4;
        dataStart = 8;
    } else if (length === 127) {
        maskStart = 10;
        dataStart = 14;
    } else {
        maskStart = 2;
        dataStart = 6;
    }
    const masks = data.slice(maskStart, maskStart + 4);
    const message = data.slice(dataStart);
    for (let i = 0; i < message.length; i++) {
        message[i] ^= masks[i % 4];
    }
    return message.toString();
}

function broadcastMessage(message) {
    const frame = createFrame(message);
    clients.forEach((client) => {
        client.write(frame);
    });
}

function createFrame(message) {
    const messageBuffer = Buffer.from(message);
    const length = messageBuffer.length;
    let frame;
    if (length <= 125) {
        frame = Buffer.alloc(2 + length);
        frame[1] = length;
        messageBuffer.copy(frame, 2);
    } else if (length <= 65535) {
        frame = Buffer.alloc(4 + length);
        frame[1] = 126;
        frame.writeUInt16BE(length, 2);
        messageBuffer.copy(frame, 4);
    } else {
        frame = Buffer.alloc(10 + length);
        frame[1] = 127;
        frame.writeBigUInt64BE(BigInt(length), 2);
        messageBuffer.copy(frame, 10);
    }
    frame[0] = 129;
    return frame;
}
