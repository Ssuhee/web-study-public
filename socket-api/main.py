import socket
import threading
import base64
import hashlib

clients = []

def handle_client(client_socket):
    request = client_socket.recv(1024).decode('utf-8')
    headers = parse_headers(request)
    accept_key = create_accept_key(headers['Sec-WebSocket-Key'])
    response = (
        'HTTP/1.1 101 Switching Protocols\r\n'
        'Upgrade: websocket\r\n'
        'Connection: Upgrade\r\n'
        f'Sec-WebSocket-Accept: {accept_key}\r\n'
        '\r\n'
    )
    client_socket.send(response.encode('utf-8'))

    clients.append(client_socket)

    while True:
        try:
            message = receive_message(client_socket)
            if message:
                broadcast_message(message)
            else:
                remove_client(client_socket)
                break
        except Exception as e:
            print(f"Error: {e}")
            remove_client(client_socket)
            break

def parse_headers(request):
    headers = {}
    lines = request.split('\r\n')
    for line in lines:
        if ': ' in line:
            key, value = line.split(': ', 1)
            headers[key] = value
    return headers

def create_accept_key(key):
    magic_string = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
    sha1 = hashlib.sha1((key + magic_string).encode('utf-8')).digest()
    return base64.b64encode(sha1).decode('utf-8')

def receive_message(client_socket):
    try:
        data = client_socket.recv(1024)
        if not data:
            return None
        length = data[1] & 127
        if length == 126:
            length = int.from_bytes(data[2:4], 'big')
            masks = data[4:8]
            message = data[8:8+length]
        elif length == 127:
            length = int.from_bytes(data[2:10], 'big')
            masks = data[10:14]
            message = data[14:14+length]
        else:
            masks = data[2:6]
            message = data[6:6+length]

        decoded_message = bytearray([message[i] ^ masks[i % 4] for i in range(length)])
        return decoded_message.decode('utf-8')
    except Exception as e:
        print(f"Error receiving message: {e}")
        return None

def broadcast_message(message):
    for client in clients:
        try:
            client.send(encode_message(message))
        except Exception as e:
            print(f"Error sending message: {e}")
            remove_client(client)

def encode_message(message):
    encoded_message = bytearray()
    encoded_message.append(129)
    length = len(message)
    if length <= 125:
        encoded_message.append(length)
    elif length <= 65535:
        encoded_message.append(126)
        encoded_message.extend(length.to_bytes(2, byteorder='big'))
    else:
        encoded_message.append(127)
        encoded_message.extend(length.to_bytes(8, byteorder='big'))
    encoded_message.extend(message.encode('utf-8'))
    return encoded_message

def remove_client(client_socket):
    if client_socket in clients:
        clients.remove(client_socket)
        client_socket.close()

def start_server():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('127.0.0.1', 65432))
    server_socket.listen(5)
    print("Server started on ws://127.0.0.1:65432")

    while True:
        client_socket, addr = server_socket.accept()
        print(f"Connection from {addr}")
        client_handler = threading.Thread(target=handle_client, args=(client_socket,))
        client_handler.start()

if __name__ == "__main__":
    start_server()
