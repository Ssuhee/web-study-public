## Test
```bash
# 1) Open index.html file on the browser
#    Options:
#       - use liveserver on vsc (recommend)
#       - apache or other server

# 2-1) Run websocat
./websocat -t ws-l:127.0.0.1:8000 broadcast:mirror:

# 2-2) Run websocat
node main.js

# 2-3) Run websocat
python3 main.py
```