document.addEventListener("DOMContentLoaded", () => {
  const baseURL = "https://jsonplaceholder.typicode.com";
  const options = [
    { name: "None", getItem: () => {}, setItem: () => {} },
    {
      name: "Local-Storage",
      getItem: () => {
        return localStorage.getItem("local-storage") || "[]";
      },
      setItem: (value) => {
        return localStorage.setItem("local-storage", value);
      },
    },
    {
      name: "Session-Storage",
      getItem: () => {
        return sessionStorage.getItem("session-storage") || "[]";
      },
      setItem: (value) => {
        return sessionStorage.setItem("session-storage", value);
      },
    },
    {
      name: "IndexedDB",
      setItem: () => {},
      getItem: () => {
        const link = document.createElement("a");
        link.href = "https://mdn.github.io/dom-examples/to-do-notifications/";
        link.target = "blank";
        link.click();
      },
    },
  ];

  const selectElement = document.querySelector("select");
  const postListElement = document.querySelector("#post-list");
  const postTemplate = document.querySelector("template#post");
  const statusElement = document.querySelector("footer > p");

  const api = (method, url) =>
    new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open(method, baseURL + url);
      xhr.responseType = "json";
      xhr.onload = () => resolve(xhr.response);
      xhr.onerror = () => reject(xhr.response);
      xhr.send();
    });

  const loadPost = (posts = []) => {
    posts.forEach(({ title, body }) => {
      const item = postTemplate.content.cloneNode(true);
      item.querySelector("[data-title]").textContent = title;
      item.querySelector("[data-body]").textContent = body;
      postListElement.append(item);
    });

    statusElement.textContent = `Status: ${postListElement.children.length}`;
  };

  const handleSelectChange = ({ target: { value } }) => {
    const { getItem, setItem } = options[value];
    let posts = getItem();
    if (posts === undefined) return;

    posts = JSON.parse(posts);

    if (posts.length !== 0) {
      return loadPost(posts);
    }

    api("GET", "/posts")
      .then((posts) => {
        loadPost(posts);
        setItem(JSON.stringify(posts));
      })
      .catch((e) => alert(e));
  };

  const loadOptions = () => {
    options.forEach(({ name }, key) => {
      const option = document.createElement("option");
      option.value = key;
      option.textContent = name;
      selectElement.append(option);
    });

    selectElement.removeEventListener("change", handleSelectChange);
    selectElement.addEventListener("change", handleSelectChange);
    selectElement.dispatchEvent(new Event("change"));
  };

  loadOptions();
});
